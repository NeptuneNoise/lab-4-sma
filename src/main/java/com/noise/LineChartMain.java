package com.noise;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.stage.Stage;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;


public class LineChartMain extends Application {

    @Override public void start(Stage stage){
        double index = 0;
        double [] smadata = new double[0];
        System.out.println("Скользящая средняя");
           try{
               do {
            Path path = Paths.get("smadata.txt");
            smadata = Files.lines(path)
                    .flatMap(e -> Stream.of(e.split(" ")))
                    .mapToDouble(Double::parseDouble)
                    .toArray();
                System.out.print(smadata[(int) index++] + " ");
            } while (index < smadata.length);

        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        Main.MyArrays ya = new Main.MyArrays();
        double[] yArray = ya.setArray(ya.Yarray);
        double[] xArray = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25};
        double[] xArray2 = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25};
        stage.setTitle("Скользящая средняя SMA");
        final NumberAxis xAxis = new NumberAxis();
        final NumberAxis yAxis = new NumberAxis();
        xAxis.setLabel("");
        yAxis.setLabel("");
        final javafx.scene.chart.LineChart<Number,Number> lineChart = new javafx.scene.chart.LineChart<Number,Number>(xAxis,yAxis);
        XYChart.Series series1 = new XYChart.Series();
        series1.setName("Исходные данные до обработки");
        for(int i = 0; i<xArray.length; i++) {
            series1.getData().add(new XYChart.Data(xArray[i], yArray[i]));
        }
        XYChart.Series series2 = new XYChart.Series();
        series2.setName("Скользящая средняя SMA");
       for(int i = 0; i<xArray2.length; i++) {
            series2.getData().add(new XYChart.Data(xArray2[i], smadata[i]));
        }
       Scene scene  = new Scene(lineChart,800,600);
        lineChart.getData().addAll(series1,series2);
        series1.getNode().setStyle("-fx-stroke: #5e42ff;-fx-fill: #c2adff;");
        series2.getNode().setStyle("-fx-stroke: #00ff00;");
        stage.setScene(scene);
        stage.show();
    }
    //-fx-fill: #c1ffba;

    public static void FileRead(File file1) throws IOException {
        System.currentTimeMillis();
        System.currentTimeMillis();
        File file = new File("smadata.txt");
        FileReader fr = new FileReader(file);
        FileReader fr1 = new FileReader(file1);
        BufferedReader reader = new BufferedReader(fr);
        String line = reader.readLine();
    }

    public static void main(String[] args) {
        launch(args);

    }

    public void start() {
        launch();
    }
}