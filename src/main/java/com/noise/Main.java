package com.noise;

import org.apache.commons.lang3.ArrayUtils;

import java.io.*;
import java.util.*;
import java.util.LinkedList;
import java.util.Queue;

public class Main {
    public final Queue<Double> window;
    public final int period;
    public double sum;
    public static Scanner input;
    public static Scanner cont;
    public static double avg(double sum, double wind) {return sum / wind;}


    public Main(int period) {
        assert period > 0;
        this.period = period;
        window = new LinkedList<Double>();
    }

    public void newNum(double num) {
        sum += num;
        window.add(num);
        if (window.size() > period) {
            sum -= window.remove();
        }
    }

    public double getAvg() {
        double Avg;
        double wind;
        if (window.isEmpty()) return 0.0;
        wind = window.size();
        Avg = avg(sum, wind);
        return Avg;
    }
    public static double window(double set){
        double window = 0;
        for (int i=0; i<set; i++)
        {
            window =i+1;
        }
        return window;
    }

    public static class Return {

        public double[] CreateSma() throws IOException {
            BufferedReader key = new BufferedReader(new InputStreamReader(System.in));
            int windowSize = 0;
            System.out.println("Введите размер окна:");
            input = new Scanner(System.in);
            String f = key.readLine();
            try {
                windowSize = Integer.parseInt(f);
            } catch (NumberFormatException e) {
                System.err.println("Ошибка ввода");
                System.out.println("Для рестарта введите: 'Yes' \nДля выхода нажмите любую клавишу");
                cont = new Scanner(System.in);
                String error;
                error = cont.next();
                if (error.equals("Yes")) {
                    new Main.Return().CreateSma();
                } else {
                    System.out.println("Работа завершена, Спасибо!");
                    System.exit(0);
                }
            }
            MyArrays ya = new MyArrays();
            double[] y = ya.setArray(ya.Yarray);
            Main ma = new Main(windowSize);
            for (double x : y) {
                ma.newNum(x);
                List<Double> sma = new ArrayList<Double>();
                int i;
                for (i = 0; i < 1; i++)
                    sma.add(ma.getAvg());
                System.out.println("Исходные данные до обработки = " + x);
                System.out.println("SMA = " + ma.getAvg());
                Double[] array = sma.toArray(new Double[sma.size()]);
                double[] doublesma = ArrayUtils.toPrimitive(array);
                PrintWriter outputFile = new PrintWriter(
                        new FileWriter("smadata.txt", true));
                try {
                    for(int a = 0; a< doublesma.length ; a++){
                        outputFile.println(doublesma[a]);
                    }
                } finally {
                    outputFile.flush();
                    outputFile.close();
                    }
            }
            return y;
        }
    }
    public static void main(String[] args) throws IOException {

        new Return().CreateSma();
    }
        public static class MyArrays {

            double[] Yarray = {17, 11, 20, 9, 41, 33, 54, 22, 34, 11, 9, 2, 7, 15, 13, 16, 9, 23, 42, 35, 39, 24, 14, 23, 42};
            double[] Yarr1;

            double[] setArray(double[] yarr) {
                Yarr1 = yarr;
                return Yarr1;
            }
        }
    }
