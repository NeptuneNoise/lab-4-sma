import com.noise.LineChartMain;
import com.noise.Main;
import org.junit.*;
import org.junit.rules.ExpectedException;


import java.io.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class Testl4 {

    @Rule
    public ExpectedException thrown = ExpectedException.none();
    @BeforeClass
    public static void start() {
        System.out.println("Запуск тестов...");
    }
    @Test
    public void FileTest(){
        Assert.assertTrue(new File("smadata.txt").exists());
    }
    @Test
    public void FileNotFoundExceptionTest(){
        Exception exception = assertThrows(Exception.class, () -> LineChartMain.FileRead(new File("not_exist_file.txt")));
        Assert.assertTrue(exception instanceof FileNotFoundException);
    }
    @Test
    public void testFileWriting() throws IOException {
        PrintWriter outputFile = new PrintWriter(
                new FileWriter("smadata.txt", true));
        outputFile.append("19.866666666666667 ");
        outputFile.flush();
        outputFile.close();
    }
    @Test
    public void TestAvg(){
       Assert.assertEquals(25.6, Main.avg(640, 25),0);
       Assert.assertEquals(0.0390625, Main.avg(25, 640),0);
       Assert.assertEquals(-0.7575757575757576, Main.avg(-25, 33),0);
    }
    @Test
    public void TestSetWindow(){
        double setWindow=Main.window(10);
        assertEquals(10, setWindow);
    }

    @AfterClass
    public static void finish() {
        System.out.println("Тесты Завершены...");
    }
}
